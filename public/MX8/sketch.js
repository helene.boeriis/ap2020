
var mood = []; // the array from the json file
var howIfeel;
var currentMood = [];
var thisSituation = 0;
var feelings;
var thoughts;

var x = []; //array of x-value for the words
var y = []; //array of y-value for the words
x = [0,10,30,60,80,100,150,250,280,320,350, 380, 420, 450, 480, 500,520,530,550,570,590,620,640,660,690,720,735,750]; //random x start value for the words
y = [0,50,100,130,200,250,300,350,360,370, 400,420,415,500,550,580,600,620,640,642,670,678,680,700,710,740,755,780,790]; ////random y start value for the words

function preload(){
  //preload json file
  howIfeel = loadJSON("wordsOfMyFeelings.json");
  thoughts = loadSound("assets/soundofmythoughts.wav");
}

function setup() {
  createCanvas(800, 800);
  frameRate(1);

}
function draw() {

  background(50);
  fill(255);
  textSize(20);
  textAlign(CENTER, CENTER);

//Headline explaining what to do
  text("Pick words to express your current mood", width/2, 15);
//box where the clicked words are placed
  fill(255);
  rect(0, 700, 800, 35);

//Print sentence on the screen
  fill(255,0,0);
  textSize(20);
  text(currentMood,400,715);

//The function myfeelings is called
  myFeelings();

}

function myFeelings(){
//Refer to tekst array in the json-file
  mood = howIfeel.rightNow;

//words printet in white colour
  fill(255);

//For loops with the words form the json file array. max 25 words on the screen
  for(let i = 0; i<25; i++){
    feelings = mood[i];


  //the x & y positions of the words change with a random amount
    x[i]+=random(1,20);
    y[i]+=random(1,20);


  //Print the words of the canvas
  text(feelings, x[i],y[i]);

    /* each time a word reaches the edges of the canvas,
    they move in the canvas again from the opposite site */
    if(x[i]>width){
      x[i]=0;
    }

    if(y[i]>height){
      y[i]=0;
    }
  }
}


//When a word is clicked, it has to be printed on the canvas
function mousePressed(){
  push();
thoughts.play();
//noLoop();
pop();
  for(let i = 0; i<25; i++){
    if (mouseX < (x[i] + 20) && mouseX > (x[i]-20)) {
      if(mouseY < (y[i]+20) && mouseY > (y[i]-20)) {
        currentMood[thisSituation]=mood[i];
        thisSituation++;

      }
    }
  }
}
