OBS: For the MX2 I have made a folder for each of the sketches, but all links is located in this folder (MX2). 

Link (RUNME) emoji(1)

https://helene.boeriis.gitlab.io/ap2020/MX2/

Link - folder

https://gitlab.com/helene.boeriis/ap2020/-/tree/master/public/MX2

![Screenshot](miniex2.png)

____

Link (RUNME) emoji(2)

https://helene.boeriis.gitlab.io/ap2020/MX2.1/

Link - folder

https://gitlab.com/helene.boeriis/ap2020/-/tree/master/public/MX2.1

![Screenshot](miniex2.1.png)



**Emoji(1)**

**Description:**

My second program illustrates an emoji. When the mouse is pressed inside the canvas
the emoji changes its skin color from brown to white. I mostly used ellipses to shape
the head, eyes and eyeballs. The tongue is made by a rect and the mouth with an arc using QUARTER_Pi. 
The function mouseispressed(); is used to make it possible to switch between the two faces and skin colors. 
Brown being the default color and white being the secondary color when the function is used. 
My intention was to create an emoji, where the default color was brown and the secondary 
color white, using a function to swap between the two colors. 
The white emoji is sticking its tongue out. A small variation for making it clear, 
that the emoji is changing when using the mouseispressed(); function. 
The program contains a function that I have not used before. 
My knowledge and literacy have been increased by challenging myself using 
interactive functions and more advanced syntaxes. It is my assumption that 
I have acquired a better understanding of how to read the code and execute debugging on my own work
and other peoples work.

**Cultural context:**

With reference to wider cultural context that concerns representations of race, 
identity, socials, culture and politics, it is clear that, my emoji(1) would not 
be acceptable in contemporary society. Obviously not all skin colors are represented 
in my emoji and the default skin color of the emoji is brown, and you are not able 
to freeze the white emoji. Taking it in the cultural aspects, the emoji is only 
a representation of people of colors, since you are not able to see the white skin color and freeze it. 
In the way the whole conflict about people of color, did not fell represented of the yellow base of the emoji,
my emoji has turned this conflict around by only being a representation
of them who didn’t felt represented before. 

On the other hand, the emoji’s eyes are blue, so actually the people of color
are not represented  fully and the whites either. I do not think 
that it overshadows the message of the emoji.



**Emoji(2) - Back to basic**

**Description:**

My emoji(2) is called ‘Back to basic’. I have used the smiley face ‘x D’ and 
combined it with one of the most popular emojis in 2019 called “Face with tears of joy”. 
My emoji is very simple, and it did not take me a long time to create the shapes.
I have used text for the ‘X D’ and then afterwards turned it around using translate(); to move the zero. 
Afterward I used rotate(); to turn it 90 degrees. 
Both translate(); and rotate(); was new to me, but are both very useful syntaxes for further sketches. 

**Cultural context:**

I have used a blue color for the face of the emoji, so it does not represent any race.
With the eyes and mouth using the ‘XD’ is does not illustrates a specific race, but only
a laughing face. Seen from an aesthetic perspective the emoji does not fit into
the detailed and realistic emojis we know today, so it would not be acceptable in contemporary society either. 
The purpose of the emoji was to make an emoji without offending anyone, 
which may also be the reason why the emoji ended up so alternative. 
