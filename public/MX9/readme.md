![SCREENSHOT](miniex9.png)

**Please check the HTML / INDEX file as well. **

**What is the program about? which API have you used and why?**


Our program is based on an API from the Giphy website, and therefore the data we get is gifs. The users of our program can interact with it by writing their name in the input box, and thereby get their internet personality shown in gifs, based on their name. Our program is very simple and consists of a header, that introduces the user to the concept, an input box, where the users write their names, and two buttons, one that searches for the gifs, and one that reloads the page and clears the screen.
We choose the API from the Giphy website, because it could help us facilitate our conceptual idea about the user's internet personality. In addition to that it gave the program a silly touch, and made it more entertaining to the user.  
The conceptual idea that the program generates the user's internet personality reflects upon the fact that the internet captures so much data about you and even connects it to your name, that it knows your internet personality. In some way criticizing the data capture like this, is kind of paradoxical, as using this program also generates data to the Giphy website, when using their API. The internet personality shown in our program with gifs might not mirror reality, just like the advertisements and recommendations shown to you on different websites, are not always of your interest.
The Giphy website structures and chooses the data we get, and the data is real-time, and can change in our program whenever the data from the Giphy website changes, which means that the Giphy website actually can control the content in our program and can limit our use of their API. With our program we want to question the query, in the way that we want to reflect upon how the gifs that appear are picked out. 


**Can you describe and reflect on your process of making this mini exercise in terms of acquiring, processing, using and representing data?**


We started out the process by having conversations about API’s. We discussed the functions and how the API’s reflect the data flow and infrastructure of the WWW. We then discussed how the API’s connect different accounts making it possible to accumulate a lot of information on the same person. At the same time, making it possible to catch, collect, metrify and store a lot of information and clicks, making it into big data. From this discussion, an idea about an internet personality and internet information appeared. We wanted to make a program, where the user could write ones name and based on the name, information would appear. We wanted to have a description from Urban Dictionary, but unfortunately the API from UD was very hard to get a hold of. We then tried to manage the API from Giphy, with a little help from Daniel Shiftman, and we succeeded in making the API work and provide data for us.
At the beginning we wanted to represent one gif for every name, and thereby only using a small bit of the information that we were querying. But then we decided to make it possible to see all the gifs that the API provided for the specific name, thereby representing more of the data that we are querying.


**How much do you understand this data or what do you want to know more about?**


The data that we have been giving was hard to grasp in the beginning. We struggled understanding the url, and the API key and the extra opportunities, in our case the search function that we had to incorporate in the URL. The data that we have been given is a JSON file with 25 different gifs from the website Giphy. 25 gifs for each search word. The gifs are the first 25 gifs that appear when you search for a specific word on the Giphy website. In addition to the gifs that we got from the API, we got a lot of further information about the specific gif, and this information is hard to understand fully, and might not be as useful to us.

**How do platform providers sort the data and give you the selected data?**

When signing up at Giphy.com you have to accept and read their User Terms of Service to point out the terms of using, creating and distributing your own and other people’s gifs. To secure the content on Giphy, there is a set of Community Guidelines users are asked to follow when using Giphy. These guidelines outline how gifs with violence, pornography, animal cruelty, hate speech and so on, are differently not allowed on the website likewise they recommend you to report these types of Giphy, so it can be removed asap.
With the use of the Giphy API, we can search on every word and a GIF with a matching hashtag will be shown. The shown GIF is selected from the hashtag and how it is ranked at Giphy.com.
We can load gifs to a limit of 25 gifs. With it, we haven’t access to all of the gifs with a given hashtag by which Giphy ensures that the data that are distributed through their API are validated content that agrees to the guidelines. However, there is still a risk of illegal content on their platform when there isn’t a screening of the gifs before uploading it and the removal will take place afterward, even though the possibilities for this illegal content that are distributed through the API is selected from a ranking system and so saying approval from Giphy.


**What are the power-relations in the chosen APIs?**


The power-relation between us as users of Giphy’s API and Giphy as data distributor is thus that we firstly need to have an API-key to access the JSON file in which their data is distributed. This is mainly because Giphy then can track when and how much we use their data in our own program. Ghiphy has reserved the right to deny or revoke our access to their API at any time and limit our frequency and number of API-resquests. However, we do not pay for the API money wise. 
((((The exchange of data via API is a two way exchange, but it is not easy to figure out what we as users of the API give in exchange. Questions about the exchange can be asked; is it a fair deal? What are we exchanging? Do we give more data than we have been provided? What forms of exchange are happening and do the users have a right to know? The power relations between the API user and the distributor can in some instances be asymmetrical. It is hard for the user to know exactly what we are exchanging when using the API, leaving the knowledge to the distributor, creating an asymmetrical exchange. )))


**What is the significance of APIs in digital culture?**


The significance of API’S in the digital culture is unavoidable in the sense that data distribution and sharing between online platforms is way easier with API’s than hardcoding data from device to device. In this regard, programming with API’s has a modular approach which makes it uncomplicated to construct a program of different pieces of data borrowed by an API. 
API-keys offers a more transparent relation between data distributor and user seen from the data distributor’s side. In that sense it simple for the distributor to get an overlook of who uses the data and in what frequency. Also, it is straightforward for the distributor to establish control among the data distributed and by that reserve the rights of the data that is shared. 



**Try to formulate a question in relation to web APIs or querying/parsing processes that you want to investigate further if you have more time**


- What kind of data are we providing Giphy with by using their API? What terms are we agreeing to, and how do we find out what we are giving in exchange for using the API? 
- How do Giphy track / or can they track how we are using their data?



[RUNME](https://helene.boeriis.gitlab.io/ap2020/MX9/)


[FOLDER](https://gitlab.com/helene.boeriis/ap2020/-/tree/master/public/MX9/)