OBS: The link can't run the program and is only turning on the camera, sorry :(
You have to run the program in ATOM, instead. 


RUNME:
https://helene.boeriis.gitlab.io/ap2020/MX4/

Link - folder
https://gitlab.com/helene.boeriis/ap2020/-/tree/master/public/MX4/


"Star qualifications"

Description

My fourth mini ex is called ’Fame Qualifications’ and is based on the estimated amount of likes your face will generate. Using this program (if it was real) you will get a fast and quick feeling of how well or bad your face will manage on social media. The program is offering a quick shortcut to your star qualities and you are not forced to use a lot of hours on Instagram, TikTok, Facebook etc. to build up your popularity. By letting the program scan your face, you will get an estimated result, showing how many likes your face it worth on social media.

The program is consists of a button, a webcam and a clm.trackr, which purpose are to recognize a face when It is shown in front of the webcam. When the clm.trackr has registered a face, you can click the button and receive an immediate input, showing the amount of likes your face it worth. It is possible to press the button before, so the clm.trackr and the button isn’t connected at that point. Afterwards, you can press the button again to get another result.  
Seen from a technical perspective the given number of likes is a dataset I have chosen, and the program will select and show a result randomly from the array in the top. 
I have learned how to set up my own functions and how to create an output when pushing a button. Furthermore I have downloaded a new library (clm.trackr) and used it in my program. 
I have placed a heart in front of the likes to make it clear for the user what the value of the numbers is. I could have used the thumps up emoji, but I found it more suitable to use a heart. I have not created the heart emoji myself, since the global understanding of emojis is pretty general. I found it here: https://editor.p5js.org/saberkhan/sketches/9YxbMpF5Z 
I have used the syntax “arc();” before in my throbber and I am fully aware of the parameters as well, but I would rather spend my time working with syntaxes and functions I have not used before.

According to ‘CAPTURE ALL’, this program is attached to increasing the productivity of everyday lives, since the program defines your potential to become a social media phenomenon and cuts off the implications it may cause.
The program is converting your face into likes/data and put aside other values, which is supporting the mentioned datafication. An unnecessary program, which contribute to the datafication and the illusion of the unlimited control it causes. 

![SCREENSHOT](miniex4.png)

