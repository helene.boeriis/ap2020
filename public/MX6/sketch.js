var cupcakeSize = {
  w:100,
  h:100
};
var bg
var screen = 0;
var score = 0;
var cherry1



function preload() {
  bg = loadImage('assets/background.png');
  cupcake = loadImage('assets/cupcake.png');
  cherry = loadImage('assets/cherry.png');

}
function setup() {
  createCanvas(1200, 800);
cherry1 = new Cherry (cherry);

}

function draw() {

if(screen == 0){
  startScreen()
}else if (screen == 1) {
  gameON()
}else if (screen==2){
  endScreen()

 }

}

function startScreen(){

background(255, 145, 202);
fill(232, 70, 154);
textSize(20);
text('CATCHING CHERRYS', width / 2 - 120, height / 2 - 60);
text('CLICK TO START', width / 2 - 100, height / 2);
reset(); //the screen will be shown again when losing the game and the mouse isessed
}

function gameON(){
  background(bg);
  fill(232, 70, 154);
  textSize(20);
  text("score = " + score, 30,20)

cherry1.show();
cherry1.move();


  image(cupcake,mouseX,700,cupcakeSize.w,cupcakeSize.h);//the cupcake

}

function endScreen(){ //when the game is finished
		background(255, 145, 202)
    fill(232, 70, 154);
		textAlign(CENTER);
    textSize(20);
		text('GAME OVER', width / 2, height / 2)
  	text("SCORE = " + score, width / 2, height / 2 + 20)
		text('CLICK TO PLAY AGAIN', width / 2, height / 2 + 40);
}

function mousePressed(){
	if(screen==0){
  	screen=1
  }else if(screen==2){
  	screen=0
  }
}

function reset(){ //when resting the game the speed and score will reset
	  score=0;
  	speed=2;
  	cherry1.y=-20;
}

class Cherry {
  constructor(helene) {
      this.helene = helene;
      this.x = 200;
      this.y = 60;
      this.size = {
        w: 50,
        h: 50
      };
      this.speed = 2;
  }

  show(){
    image(this.helene,this.x, this.y, 50, 50);//if i wrote this.size.w and this.size.h it didn't worked, please let me know if you can make it work hehe
  }

  move(){

    this.y+= this.speed;//speed for the first cherry
    if(this.y>height){
      screen =2 //if the cherry isn't catched show game-over-screen
     }
    if(this.y>height-20 && this.x>mouseX-38 && this.x<mouseX+38){ //postion of the cupcake (mouseX) and the cherry so the program knows that it (cherry) has been catched
      this.y=-20
      this.speed+=.5 //turing up the speed every time a cherry is catched
      score+= 1 //adding a point to the score every time a cherry is catched
    }
    if(this.y==-20){
      this.pickRandom();
    }
  }
  pickRandom(){ //the game selects a random start position of the new cherry

  	this.x= random(20,width-20)
  }
}
