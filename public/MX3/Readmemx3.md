RUNME: 

https://helene.boeriis.gitlab.io/ap2020/MX3/

Link - folder

https://gitlab.com/helene.boeriis/ap2020/-/tree/master/public/MX3

![SreenShot](mx3.png)

My third mini ex illustrates a throbber, made by an arc();. The throbber is not
that complicated or different from other throbbers. When it reaches the starting
point, its start over until the loading is finished. As usual it does not give
the user any kind of information.
For my point of view, a throbber symbolizes a processing computer or browser,
trying to process a request. While the throbber spins it visually indicates
actions are in progress and the actual handling of data does not have a direct
relation with how the throbber is animated visually. It is only demonstrating a
regular tempo, which have nothing to do with what is happening. One the other
hand the speed of the throbber in some way denotes how fast the computer,
browser or feed is processing. With that in mind, the speed of my throbber is
very similar to other well-known throbbers and the expression and configuration
is basically the same, instead of turning up speed or slowing it down. 

I have used some of the syntaxes from a similar throbber work, mostly, how to 
get the arc(); spin using variables and ‘if’ statements.

Link: https://creativecoding778.wordpress.com/2017/12/07/creating-a-throbber/

