**Challenges between simplicity at the level of commnucation and complexity at the level of algorithmic procedure** 
 
I have composed a flowchart of my mini ex 8 where words from a JSON file are shown on the canvas. When a word is pressed, a sound will play, and the selected word will be posted in a box at the bottom of the page. 
One of the challenges might have been what the syntax, in the code, actually do and how it behaves. Furthermore, to translate the complexity of the used syntaxes into an understandable general language. It puts the program into perspective.  
It was quite difficult to make this description in the flowchart without referring to other syntaxes and compose an explanation of how the different syntaxes are "calling" each other and how it affects the program.
 
**The technical challenges for the two ideas and how are you going to address them?**
 
The technical challenges for the two ideas for the final project have been to create flowcharts of "half-finished" projects. 
We considered both the ideas as finished to make a reliable flowchart, while keeping in mind that there is space for improvements. It has also been challenging to think and discuss the performance of the program in relation to our message with the program.  

**Value of the individual and the group flowcharts**
 
It is hard to compare the two flowcharts we made in the group, and the one I made individually. The individual flowchart has been made of an existing program and the other two flowcharts has been made of new ideas.
The value of the individual flowchart has been made to experience the complexity of a program, where you may not have been clear about the final result of the program, when starting to code it. We (Annika and I) did not follow a flowchart/workplan, so parts of the program were learning by doing. In that situation it may have been useful with a flowchart, so the flow of the program was clear. 
The flowchart we made with the group was a little different because our expectations and ideas had to be the result of a mutual program. The way the program should perform was something we had to consider and agree on, to make a flowchart together. From my point of view, I think that the flowchart helped us to agree on the performance of the program, which is essential for a good groupwork dynamic during the final project. 
The values have been to experience how hard it is the translate something so complex as syntaxes into simple and understandable text. 

![SCREENSHOT](miniex.png)

Group flowchart
https://gitlab.com/Signe.Tvilling/ap-2020/-/tree/master/public/MiniX10

Link - folder
https://gitlab.com/helene.boeriis/ap2020/-/tree/master/public/MX10/