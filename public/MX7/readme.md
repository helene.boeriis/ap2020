![SCREENSHOT](miniex7.png)

My mini ex 7 is a generative program with an array of bubbles/ellipses. 
I have made a class for the bubbles where I have defined their parameters. 
The bubbles appear randomly on the canvas with a radius between 1 and 6. 
The speed of the bubbles will also be picked randomly by the program between 0.2 and 3. 
I have also used the random function for their x and y position.
So, four of the parameters in the class are picked randomly by the program.  
With so many bubbles it is actually very difficult to see the different 
pattern and movement, when refreshing the page. 
Some of the bubbles are dragged to the 0,0 of the axes and disappears.  
Some of them are just moving around on the canvas and but will slowly disappear as well. 
And some will stay forever.I have also added a function, where to bubbles change 
its color when passing the middle of the canvas.

I found it hard to define what randomness is for me and how I could express it in my program. 
The mathematicians and computer scientists address randomness to be defined 
by a set of (can be known) values where the output is unpredictable, but you are 
still aware of the possible output. So, when the x and the y values of my bubbles 
are picked randomly by the program, I still know that the bubbles will be inside 
the canvas because I told the program to. I set a rule.
According to the text 10 prints text, randomness in contemporary 
computing is also used for generating a random password. But there will still 
be a set of rules the program has to stick to; fx they can’t generate a password 
which already has been created. The “generator” has a set of rules, 
like the password must contain x number of numbers and x number of characters or even a sign. 

With that in mind, you still have an idea of the outcome, because of the rules. 
When generating a random password, the generator picks a combination from a set of rules and defined values. 
Is it random then?
Randomness in this case (my program and the password generator) depends on how you 
interpret the aspects of the processes and what you attribute to randomness.
There is a lot of randomness in my program, but somehow it is still predictable. 
So, if there were more rules when it is more unpredictable then? For my point of view, 
there will just be more possible outcomes but still predictable. 



[RUNME](https://helene.boeriis.gitlab.io/ap2020/MX7/)



[FOLDER](https://gitlab.com/helene.boeriis/ap2020/-/tree/master/public/MX7/)