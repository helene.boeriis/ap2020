<<<<<<< HEAD

=======
My artwork illustrates a sitting cat only made by circles, lines and ellipses. 
The circles illustrate the body, head, paws and tail of the cat. The lines are used as whiskers and the ellipses as ears.  
My first thought was to make a cat, only using circles, because the syntaxes and parameters of the circle is understandable and easy to work with.

First, the syntax and parameters weren’t immediately obvious, but after reading the descriptions at p5.js, it started to make sense. 
Combined with changing the number in the syntax and seeing the alteration in the browser, 
I started to figure it out. I was now able to make a circle and not just feed in random data for the sizes and positions, 
but actually entering data in the syntax with a known output. Afterwards, I moved on making the ears of the cat using a triangle. 
A Triangle have six parameters, and the syntax is therefore quite long, which makes it even more difficult than the circle. Instead, 
I used the ellipse to shape the ears, which were a little easier to work with.  

In relation to thinking, reading, modifying and copying, it has been very educational and a self-taught process, 
since we haven’t been taught how to code. Reading syntaxes, parameters and descriptions at p5.js 
has been very helpful for learning for the basics about Javascript flexibility. Copying and modifying was afterwards a little easier. 
It forced me to challenge myself in further developing my cat. After getting comfortable with line, 
circles and ellipses, the cat needed a rotating tail. The code for the rotating tail became very advanced, from my point of view, 
and it took half an hour to modify the syntaxes, so the rotation looked a little bit like a rotating cattail.  
Learning how to code is very similar to learning new languages, including the grammatic and the pronunciation of the words.
The same is in evidence with coding, where having a good literacy of syntaxes is essential for the understanding and further learning. 
Annette Vee used the words “todays literacy” when she describes coding, which describes how coding is similar to learning new languages nowadays. 
Coding and programming are a useful tool for further prototypes and gives me a basic understanding of 
how digital artefact works and what is behind the surface of an interface. 









![ScreenShot](MiniEX1.png)



https://heleneboeriis.gitlab.io/ap2020/public/MX1/
>>>>>>> master
