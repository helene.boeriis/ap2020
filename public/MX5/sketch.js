var c;
var catface;

function preload(){
  catface = loadImage('assets/cface.png');
}
function setup() {

c = createCanvas(2000, 2000);

image(catface, 332,160,310, 308);

}
function mousePressed() {
saveCanvas(c, 'APicOfHeleneAsACatUAreWelcomeBuy', 'screenshot.png', 1);

}

function draw() {
fill(0);
text('Hi, place the cursor on the cat and refresh the page (command + r). Draw a tail on the cat and click the mousebutton when you are finished',20,20);


fill(235, 183, 52);
noStroke();
ellipse(mouseX, mouseY, 30, 30);

push();

scale(9);
translate(-26,-80);

// ears
fill (235, 183, 52);
stroke(0);
ellipse(70, 95, 10, 20);
ellipse(90, 95, 10, 20);

//paws
fill(232, 187, 97);
stroke(0);
circle(88, 183, 20);
circle(72, 183, 20);


fill (235, 171, 52);
//body
circle(80, 160, 60);
// head
noFill();
circle(80, 115, 35);
pop();

push();
scale(5);
translate(45.5,20);
stroke(0);
// right whiskers
line(70, 50, 90, 50);
line(70, 45, 90, 37);
line(70, 55, 90, 63);
// left whiskers
line(30, 50, 10, 50);
line(30, 45, 10, 37);
line(30, 55, 10, 63);
pop();

}
