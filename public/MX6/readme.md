![ScreenShot](mx6.png)

Readme MX6

My miniex 6 is a game where you have to catch the falling cherries. A very simple game with elements I found cute and complementary.
Before starting the game, a screen appears with the name of the game and how to start it. The cupcake follows mouseX, which I assume you will discover quickly. Immediately after clicking the mouse the background will shift and a cherry will start falling from the top of the canvas. When the cherry is caught a new will appear a little faster than the previous. For every caught cherry you will get one point. If you miss a cherry the game is over, and your score will be shown on the screen. Afterward, you can click to play again. 
My game contains one object: the cherry. The cherry is comprised of the features I have defined in the class such as the x and y values, the size and the speed. The functionalities are to show the cherry on the screen when gameOn(); is called and makes the cherry fall from the top of the canvas and checking if the position of the mouseX matched the position of the cherry or not. 

To be honest I programmed the game without objects because I couldn’t figure how to do it. So, when I finished the program, I started to make the Cherry class and filled it with its features.
Afterward, I divided some of the lines of code from in my gameOn(); function into show (); and move(); in my cherry class. 
My cherry object is related to the cupcake, the score, and the speed. I found it hard to define the position of the mouseX and match it with the position of the object (cherry) in combination with the score and the increasing speed. 

Object-oriented programming makes it easier for programmers to design and organize software programs. When hiding unnecessary details in classes the code becomes more encapsulated and manageable. I became aware of this when I made my cherry class and filled it with the features I have written as variables and in the image(); syntax. So, when creating the class, the features were assembled. 

My cherry object is created by me, which means that its proportions, looks, and movement is made by me and my perception of how the game should behave. So, the characteristic of OOP (Object-oriented programming) must be how we define things as objects differently and it is affected by our perception of a given object. My cherry is the outcome of my perception of a cherry, where I have simplified the complexity of the object into elements, I can code. So, according to a wider cultural context, everything can be considered as objects and is not only defined by computers.


RUNME:

https://helene.boeriis.gitlab.io/ap2020/MX6/

Link - folder

https://gitlab.com/helene.boeriis/ap2020/-/tree/master/public/MX6/
