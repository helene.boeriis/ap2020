//array of bubbles
var bubbles = [];
function setup (){
createCanvas(1000,1000);
//for loop for 5000 bubbles
  for(var i = 0; i < 5000; i++){
    bubbles[i] = new Bubble();
  //  frameRate(7);
  }
}


function draw (){
background(253, 255, 156);
for(var j = 0; j < bubbles.length; j++){

bubbles[j].show();
bubbles[j].move();
 }
}


class Bubble {
  constructor(x) {

      this.x = random(height);
      this.y = random(width);
      this.r = random(1,6);
      this.speed = random(0.2,3);
      this.colr = color(239, 171, 245);
      this.colr1 = color(255, 173, 228);

}

  show(){

    noStroke();
    fill(this.colr);
    ellipse(this.x, this.y, this.r *2);

    if(this.y < width/2 || this.x < 0){
      noStroke();
      fill(this.colr1);
      ellipse(this.x, this.y, this.r *2);

 } else {

   noStroke();
  fill(this.colr);
  ellipse(this.x, this.y, this.r *2);

  }
}


  move(){

    this.x -= this.speed;
    this.x= this.x + random();
    this.y -= this.speed;
    this.y = this.y + random();
  }
}
