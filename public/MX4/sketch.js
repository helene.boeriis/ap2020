var button;
var likes = ['8','56', '2,758','17', '99','5','65,799','3','0','487','one from your mom'];
var ctrackr;
var c;
var showLike = false;
var likeCount = 0;


function setup() {

//webcam
  let capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.position(100,0);

  c = createCanvas(1000, 1000);//canvas
  c.position(100,0);//position of the canvas

//face tracker setup
ctrackr = new clm.tracker();
ctrackr.init(pModel);
ctrackr.start(capture.elt);

//button
  button = createButton('click to view how many likes your face is worth');
  button.position(490, 490);
  button.mousePressed(manylikes);

}

function draw() {
clear();

let positions = ctrackr.getCurrentPosition(); /current postion of the face
  if (positions.length) {

    for (let i=0; i<positions.length; i++) {
    //color, ellipses
       fill(map(positions[i][0], 0, width, 100, 255),212, 153, 242);
   //draw ellipse at each position point
       ellipse(positions[i][0], positions[i][1], 5, 5);

    }
  }
  if (showLike == true) {
    //color, text
    fill(0);
    //size of the text
    textSize(25);
    text(likeCount,430,550);


    //color, heart
    fill(245, 83, 83);
    noStroke();
    //the size of the heart has been scaled down
    scale(0.5);
    //postion has been moved
    translate(520,755);
    //heart
    quad(300, 300, 275, 325, 300, 350, 325, 325);
    	arc(288, 313, 35, 35, PI - QUARTER_PI, 0);
    	arc(313, 313, 35, 35, PI - QUARTER_PI, QUARTER_PI);

  }

}
function manylikes(){
  showLike = true;
  likeCount = random(likes);

  }
